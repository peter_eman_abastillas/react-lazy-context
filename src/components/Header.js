import React, { Component } from "react";
import { Auth } from "aws-amplify";
import { Link } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Menu from "@material-ui/core/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MenuItem from "@material-ui/core/MenuItem";
import { AuthContext } from "Packages/Context/AuthenticationContext";

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null,
      drawer: false
    };
  }
  handleDrawerOpen = () => {
    console.log(this);
    this.setState({ drawer: true });
  };

  handleDrawerClose = () => {
    this.setState({ drawer: false });
  };

  handleChangeAnchor = event => {
    this.setState({
      anchor: event.target.value
    });
  };

  handleLogout = async event => {
    await Auth.signOut();
    this.props.history.push("/login");
  };
  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  render() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    return (
      <div className="Header">
        <AuthContext.Consumer>
          {con => {
            const { isAuthenticated } = con.state;
            return (
              <AppBar position="static">
                <Toolbar>
                  <IconButton color="inherit" aria-label="Menu">
                    <MenuIcon onClick={() => this.handleDrawerOpen()} />
                  </IconButton>
                  <Typography
                    variant="title"
                    className="appTitle"
                    color="inherit"
                  >
                    <Link to="/">Scratch</Link>
                  </Typography>
                  <div>
                    <IconButton
                      aria-owns={open ? "menu-appbar" : null}
                      aria-haspopup="true"
                      onClick={this.handleMenu}
                      color="inherit"
                    >
                      {isAuthenticated && <AccountCircle />}
                    </IconButton>
                    <Menu
                      id="menu-appbar"
                      anchorEl={anchorEl}
                      anchorOrigin={{
                        vertical: "top",
                        horizontal: "right"
                      }}
                      transformOrigin={{
                        vertical: "top",
                        horizontal: "right"
                      }}
                      open={open}
                      onClose={this.handleClose}
                    >
                      <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                      <MenuItem onClick={this.handleClose}>My account</MenuItem>
                    </Menu>
                  </div>
                </Toolbar>
              </AppBar>
            );
          }}
        </AuthContext.Consumer>
      </div>
    );
  }
}

export default Header;
