import React from "react";
import Drawer from "@material-ui/core/Drawer";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";

export default class MainDrawer extends React.Component {
  render() {
    const { anchor = "left", open = false, closeCallBack } = this.props;

    return (
      <Drawer variant="persistent" anchor={anchor} open={open}>
        <div>
          <IconButton onClick={() => closeCallBack()}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <ul>
          <li>test</li>
        </ul>
        <Divider />
        <ul>
          <li>test</li>
        </ul>
      </Drawer>
    );
  }
}
