import React from "react";
import { Glyphicon } from "react-bootstrap";
import "./LoaderButton.css";
import Button from "@material-ui/core/Button";

export default ({
  isLoading,
  text,
  loadingText,
  className = "",
  disabled = false,
  ...props
}) => (
  <Button
    variant="outlined"
    className={`LoaderButton ${className}`}
    disabled={disabled || isLoading}
    {...props}
    fullWidth
  >
    {isLoading && <Glyphicon glyph="refresh" className="spinning" />}
    {!isLoading ? text : loadingText}
  </Button>
);
