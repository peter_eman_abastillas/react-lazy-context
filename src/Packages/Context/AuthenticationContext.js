import React, { createContext, Component } from "react";
import { Auth } from "aws-amplify";
const AuthContext = createContext({
  isAuthenticated: false,
  isAuthenticating: true,
  isLogingIn: false,
  newUser: null
});

class AuthProvider extends Component {
  state = {
    isAuthenticated: false,
    isAuthenticating: true,
    isLogingIn: false,
    newUser: null
  };

  userHasAuthenticated = (authenticated, cb) => {
    this.setState({ isAuthenticated: authenticated }, cb());
  };
  async componentDidMount() {
    try {
      if (await Auth.currentCredentials()) {
        this.userHasAuthenticated(true, () => {});
      }
      if (await Auth.currentSession()) {
        this.userHasAuthenticated(true, () => {});
      }
    } catch (e) {
      if (e !== "No current user") {
        alert(e);
      }
    }

    this.setState({ isAuthenticating: false });
  }
  handleSocialCallback = (userToken, cb) => {
    this.userHasAuthenticated(true, cb);
  };

  handleLogout = async event => {
    await Auth.signOut();

    this.userHasAuthenticated(false);

    this.props.history.push("/login");
  };
  handleSignup = async (event, email, password) => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      const newUser = await Auth.signUp({
        username: email,
        password: password
      });
      this.setState({
        newUser
      });
    } catch (e) {
      alert(e.message);
    }

    this.setState({ isLoading: false });
  };

  handleConfirmationSubmit = async (event, confirmationCode) => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await Auth.confirmSignUp(this.state.email, confirmationCode);
      await Auth.signIn(this.state.email, this.state.password);

      this.userHasAuthenticated(true);
    } catch (e) {
      alert(e.message);
      this.setState({ isLoading: false });
    }
  };
  handleLogin = async (event, email, password) => {
    event.preventDefault();

    this.setState({ isLogingIn: true });

    try {
      await Auth.signIn(email, password);
      this.userHasAuthenticated(true);
      // this.props.history.push("/");
    } catch (e) {
      alert(e.message);
      this.setState({ isLogingIn: false });
    }
  };
  render() {
    console.log(this);
    return (
      <AuthContext.Provider value={{ ...this }}>
        {this.props.children}
      </AuthContext.Provider>
    );
  }
}

export { AuthContext };
export default AuthProvider;
