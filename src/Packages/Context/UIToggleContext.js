import React, { createContext, Component } from "react";
import { Auth } from "aws-amplify";
const UIToggleContext = createContext({
  isAuthenticated: false,
  isAuthenticating: true,
  isLogingIn: false,
  newUser: null
});

class UIToggleProvider extends Component {
  state = {
    isAuthenticated: false,
    isAuthenticating: true,
    isLogingIn: false,
    newUser: null
  };
  toggleUI(name) {
    this.setState([name], !this.state[name]);
  }
  render() {
    return (
      <UIToggleContext.Provider value={{ ...this }}>
        {this.props.children}
      </UIToggleContext.Provider>
    );
  }
}

export { UIToggleContext };
export default UIToggleProvider;
