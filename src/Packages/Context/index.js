import React, { Component } from "react";

function withProvider(WrappedComponent, registerContext) {
  return class extends Component {
    wrappedComponent = null;
    render() {
      console.log(WrappedComponent);
      this.wrappedComponent = <WrappedComponent />;
      for (let name in registerContext) {
        let ContextComponent = registerContext[name];
        this.wrappedComponent = (
          <ContextComponent name={name}>
            {this.wrappedComponent}
          </ContextComponent>
        );
        console.log(name, this.wrappedComponent);
      }

      const WrappedContextComponent = this.wrappedComponent;
      return <React.Fragment>{WrappedContextComponent}</React.Fragment>;
    }
  };
}

function withContext(WrappedComponent, context) {
  return class extends Component {
    render() {
      const contextProps = context.length ? context[0] : context;
      if (context.length) {
        let WrappedConsumer = WrappedComponent;
        console.log(context);
        context.forEach(Element => {
          WrappedConsumer = (
            <Element.Consumer>
              {value => {
                const props = {
                  [value.props.name]: { ...value }
                };
                return <WrappedConsumer {...props} />;
              }}
            </Element.Consumer>
          );
        });
        return <React.Fragment>{WrappedConsumer}</React.Fragment>;
      }

      return (
        <contextProps.Consumer>
          {value => {
            let props = {
              [value.props.name]: { ...value }
            };
            return <WrappedComponent {...props} {...this.props} />;
          }}
        </contextProps.Consumer>
      );
    }
  };
}

export { withContext, withProvider };

export default withProvider;
