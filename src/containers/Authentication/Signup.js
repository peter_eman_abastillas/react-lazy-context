import React, { Component } from "react";

import Grid from "@material-ui/core/Grid";
import FacebookButton from "components/FacebookButton";
import LoaderButton from "components/LoaderButton";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import { AuthContext } from "../../Packages/Context/AuthenticationContext";
import { Auth } from "aws-amplify";
import "./Signup.css";
import { withContext } from "Packages/Context/";

class Signup extends Component {
  state = {
    isLoading: false,
    email: "",
    password: "",
    confirmPassword: "",
    confirmationCode: "",
    newUser: null
  };

  validateForm() {
    return (
      this.state.email.length > 0 &&
      this.state.password.length > 0 &&
      this.state.password === this.state.confirmPassword
    );
  }

  validateConfirmationForm() {
    return this.state.confirmationCode.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  renderConfirmationForm() {
    const { AuthContext } = this.props;

    return (
      <form
        onSubmit={e =>
          AuthContext.handleConfirmationSubmit(e, this.state.confirmationCode)
        }
      >
        <TextField
          id="confirmationCode"
          autoFocus
          label="Confirmation Code"
          shrink="false"
          type="number"
          value={this.state.confirmationCode}
          onChange={this.handleChange}
          fullWidth
          helperText="Please check your email for the code."
        />

        <LoaderButton
          block
          bsSize="large"
          disabled={!this.validateConfirmationForm()}
          type="submit"
          isLoading={this.state.isLoading}
          text="Verify"
          loadingText="Verifying…"
        />
      </form>
    );
  }

  renderForm() {
    const { AuthContext } = this.props;
    console.log(this.props);
    return (
      <form
        onSubmit={e =>
          AuthContext.handleSignup(e, this.state.email, this.state.password)
        }
      >
        <TextField
          id="email"
          autoFocus
          label="Email"
          shrink="false"
          type="email"
          value={this.state.email}
          onChange={this.handleChange}
          fullWidth
        />

        <TextField
          id="password"
          label="Password"
          margin="normal"
          value={this.state.password}
          shrink="false"
          onChange={this.handleChange}
          type="password"
          fullWidth
        />
        <TextField
          id="confirmPassword"
          label="Confirm Password"
          margin="normal"
          value={this.state.confirmPassword}
          shrink="false"
          onChange={this.handleChange}
          type="password"
          fullWidth
        />

        <LoaderButton
          disabled={!this.validateForm()}
          type="submit"
          isLoading={AuthContext.state.isLogingIn}
          text="Login"
          loadingText="Logging in…"
        />
        <Divider />
        <Grid className="Login" container>
          <FacebookButton
            buttonClass="fb-button-signup"
            callback={AuthContext.handleSocialCallback}
          />
        </Grid>
      </form>
    );
  }

  render() {
    console.log(this.props);
    return (
      <Grid className="Login" container>
        {this.props.AuthContext.state.newUser === null
          ? this.renderForm()
          : this.renderConfirmationForm()}
      </Grid>
    );
  }
}

export default withContext(Signup, AuthContext);
