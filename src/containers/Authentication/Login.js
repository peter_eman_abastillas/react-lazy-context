import React, { Component } from "react";
import { Auth } from "aws-amplify";
import Grid from "@material-ui/core/Grid";
import FacebookButton from "components/FacebookButton";
import LoaderButton from "components/LoaderButton";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import { withContext } from "Packages/Context/";
import "./Login.css";
import { AuthContext } from "../../Packages/Context/AuthenticationContext";
import { UIToggleContext } from "../../Packages/Context/UIToggleContext";

class Login extends Component {
  state = {
    email: "",
    password: ""
  };

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  render() {
    const { AuthContext } = this.props;
    console.log(this);
    return (
      <Grid className="Login" container>
        <form
          onSubmit={e =>
            AuthContext.handleLogin(e, this.state.email, this.state.password)
          }
        >
          <TextField
            id="email"
            autoFocus
            label="Email"
            shrink="false"
            type="email"
            value={this.state.email}
            onChange={this.handleChange}
            fullWidth
          />

          <TextField
            id="password"
            label="Password"
            margin="normal"
            value={this.state.password}
            shrink="false"
            onChange={this.handleChange}
            type="password"
            fullWidth
          />

          <LoaderButton
            disabled={!this.validateForm()}
            type="submit"
            isLoading={AuthContext.state.isLogingIn}
            text="Login"
            loadingText="Logging in…"
          />
        </form>
        <Divider />
        <Grid className="Login" container>
          <FacebookButton
            buttonClass="fb-button-signup"
            callback={AuthContext.handleSocialCallback}
          />
        </Grid>
      </Grid>
    );
  }
}

Login = withContext(withContext(Login, UIToggleContext), AuthContext);

export default Login;
