import { createMuiTheme } from "@material-ui/core/styles";
const overrides = {
  //
  MuiInputLabel: {
    formControl: {
      transform: `translate(20px, 37px) scale(1)`
    }
  },
  MuiGrid: {
    container: {
      padding: 20,
      margin: "20px auto 0"
    }
  },
  MuiInput: {
    input: {
      // Name of the rule
      border: "1px solid #cecece",
      borderRadius: 6,
      padding: 20
    },

    underline: {
      "&:before": {
        display: "none"
      },
      "&:after": {
        display: "none"
      }
    }
  }
};
const theme = createMuiTheme({
  palette: {
    // primary: ""
  },

  // ...overrides,
  overrides: {
    MuiButton: {
      // Name of the component ⚛️ / style sheet
      root: {
        // Name of the rule
        color: "white" // Some CSS
      }
    },
    ...overrides
  }
});

export default theme;
