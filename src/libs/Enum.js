const enums = function() {
  var o = {},
    i;

  // Initialize the enum counter as necessary
  if (typeof window.__enumCtr === "undefined") {
    window.__enumCtr = 0;
  }

  // Assign a unique value to each item, using the enum counter
  for (i = 0; i < arguments.length; i++) {
    let num = window.__enumCtr++;
    o[arguments[i]] = num;
    o[num] = arguments[i];
  }

  return o;
};
window.enums = enums;
export default enums;
