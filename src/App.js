import React, { Component } from "react";
import { Auth } from "aws-amplify";
import { Link, withRouter } from "react-router-dom";
import Routes from "./Routes";
import Typography from "@material-ui/core/Typography";
import MainDrawer from "./components/MainDrawer";
import "./App.css";
import Header from "./components/Header";
import ContextProvider, { withProvider } from "./Packages/Context/";
import UIToggleProvider from "./Packages/Context/UIToggleContext";
import AuthProvider from "./Packages/Context/AuthenticationContext";

class App extends Component {
  state = {
    isAuthenticated: false,
    isAuthenticating: true,
    anchorEl: null,
    drawer: false
  };

  handleDrawerOpen = () => {
    this.setState({ drawer: true });
  };

  handleDrawerClose = () => {
    this.setState({ drawer: false });
  };

  handleChangeAnchor = event => {
    this.setState({
      anchor: event.target.value
    });
  };

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };
  render() {
    const { anchorEl, drawer } = this.state;
    const open = Boolean(anchorEl);
    return (
      <div className="App">
        <Header openCallBack={this.handleDrawerOpen} anchorOpen={open} />
        <MainDrawer open={drawer} closeCallBack={this.handleDrawerClose} />
        <div className="container">
          <Routes />
        </div>
      </div>
    );
  }
}

export default withProvider(withRouter(App), {
  UIToggleContext: UIToggleProvider,
  AuthContext: AuthProvider
});
